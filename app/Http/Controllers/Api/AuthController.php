<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'phone' => 'required|string',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            'password_confirmation' => 'required',
            'agreement' => 'required|boolean'
        ]);

        $validatedData['password'] = Hash::make($request->password);

        unset($validatedData['passwod_confirmation']);
        unset($validatedData['agreement']);
        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['message' => 'Akunmu berhasil didaftarkan', 'user' => $user, 'access_token' => $accessToken], 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'akun' => 'required|string',
            'password' => 'required|string',
        ]);

        $akun = User::where('username', $request->akun)->first();
        if ($akun) {
            $email = $akun->email;
        } else {
            $akun = User::where('email', $request->akun)->first();
            if (!$akun) return response(['message' => 'This User does not exist, check your details'], 402);
            $email = $request->akun;
        }
        $credentials = [
            'email' => $email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            return response(['message' => 'This User does not exist, check your details'], 400);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response(['message' => 'Kamu berhasil login', 'user' => Auth::user(), 'access_token' => $accessToken], 200);
    }
}
