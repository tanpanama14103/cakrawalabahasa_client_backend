<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HubungiSaya;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'pesan' => 'required|string',
            'metode' => 'required|string',
        ]);

        $hubungi_saya = new HubungiSaya($request->all());
        $hubungi_saya->save();

        return response()->json(['message' => 'Pesan Anda berhasil terkirim'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function show(HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function edit(HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function destroy(HubungiSaya $hubungiSaya)
    {
        //
    }
}
