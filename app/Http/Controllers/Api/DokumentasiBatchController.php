<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DokumentasiBatch;
use Illuminate\Http\Request;

class DokumentasiBatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dokuemntasi_batchs = DokumentasiBatch::with(array('dokumentasi' => function($query) {
            $query->select('id', 'batch_id', 'image', 'caption', 'created_at');
        }))->get();
        return response(['dokuemntasi_batchs' => $dokuemntasi_batchs], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function show(DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function edit(DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(DokumentasiBatch $dokumentasiBatch)
    {
        //
    }
}
