<?php

namespace App\Http\Controllers\Api\Registration;

use App\Http\Controllers\Controller;
use App\Models\RegistIntHeroes;
use Illuminate\Http\Request;

class RegistIntHeroesController extends Controller
{
    private const VALIDATE = [
        'full_name' => 'required|string|unique:regist_int_heroes,full_name',
        'wa_number' => 'required|numeric',
        'nationality' => 'required|string',
        'age' => 'required|numeric',
        'status' => 'required|string',
        'language_speake' => 'required|string',
        'language_teach' => 'required|string',
        'division' => 'required|string',
        'follow_ig' => 'required|string'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(self::VALIDATE);
        RegistIntHeroes::create(request()->all());

        return response()->json(['message' => 'Your data has been sent'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistIntHeroes  $registIntHeroes
     * @return \Illuminate\Http\Response
     */
    public function show(RegistIntHeroes $registIntHeroes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegistIntHeroes  $registIntHeroes
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistIntHeroes $registIntHeroes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegistIntHeroes  $registIntHeroes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistIntHeroes $registIntHeroes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistIntHeroes  $registIntHeroes
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistIntHeroes $registIntHeroes)
    {
        //
    }
}
