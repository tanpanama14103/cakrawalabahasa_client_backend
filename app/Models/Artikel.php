<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = 'artikel';

    public function artikelFeatured()
    {
        return $this->hasMany(ArtikelFeatured::class, 'artikel_id');
    }

    public function artikelKategori()
    {
        return $this->belongsTo(ArtikelKategori::class, 'kategori_id', 'id');
    }

    protected static function booted()
    {
        // default setiap query, bila tidak ingin digunakan ->withoutGlobalScopes(['order_desc'])->get();
        static::addGlobalScope('order_desc', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function scopekategori($query, $kategori = null)
    {
        if ($kategori) {
            $query->where('kategori_id', $kategori);
        }
        
        return $query;
    }
}
