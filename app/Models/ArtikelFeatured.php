<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtikelFeatured extends Model
{
    protected $table = 'artikel_featured';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function artikel()
    {
        return $this->belongsTo(Artikel::class, 'artikel_id', 'id');
    }
}
