<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtikelKategori extends Model
{
    protected $table = 'artikel_kategori';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function artikel()
    {
        return $this->hasMany(Artikel::class, "kategori_id");
    }
}
