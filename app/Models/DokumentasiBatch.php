<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DokumentasiBatch extends Model
{
    protected $table = 'dokumentasi_batch';
    protected $hidden = ['updated_at', 'deleted_at'];

    public function dokumentasi()
    {
        return $this->hasMany(Dokumentasi::class, 'batch_id');
    }
}
