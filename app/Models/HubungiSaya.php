<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HubungiSaya extends Model
{
    protected $table = 'hubungi_kami';
    protected $fillable = ['nama', 'email', 'phone', 'metode', 'pesan', 'read_at'];
}
