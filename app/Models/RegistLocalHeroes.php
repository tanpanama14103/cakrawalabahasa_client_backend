<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistLocalHeroes extends Model
{
  use SoftDeletes;

  protected $table = 'regist_local_heroes';
  protected $softdelete;
  protected $hidden = ['updated_at', 'delete_at'];
  protected $fillable = ['nama_lengkap', 'nomor_wa', 'domisili', 'usia', 'status', 'instansi', 'divisi1', 'divisi2', 'link_portfolio', 'cv', 'follow_ig'];
  protected $guarded = ['id'];
}
