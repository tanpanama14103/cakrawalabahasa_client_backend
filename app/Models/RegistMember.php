<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistMember extends Model
{
  use SoftDeletes;

  protected $table = 'regist_member';
  protected $softdelete;
  protected $hidden = ['updated_at', 'delete_at'];
  protected $fillable = ['nama_lengkap', 'nomor_wa', 'tgl_lahir', 'alamat', 'kota', 'status', 'instansi', 'minat', 'paket', 'follow_ig', 'active'];
  protected $guarded = ['id'];
}
