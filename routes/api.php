<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('sign-up', 'Api\AuthController@register');
Route::post('sign-in', 'Api\AuthController@login');

Route::resource('artikel', 'Api\ArtikelController');
Route::post('artikel-list', 'Api\ArtikelController@list');
Route::get('artikel-populer', 'Api\ArtikelController@populer');
Route::resource('contact-us', 'Api\ContactUsController');
Route::resource('artikel-kategori', 'Api\ArtikelKategoriController');
Route::resource('artikel-featured', 'Api\ArtikelFeaturedController');
Route::resource('dokumentasi', 'Api\DokumentasiBatchController');

Route::group(['prefix' => 'regist'], function () {
    Route::resource('int-heroes', 'Api\Registration\RegistIntHeroesController');
    Route::resource('local-heroes', 'Api\Registration\RegistLocalHeroesController');
    Route::resource('member', 'Api\Registration\RegistMemberController');
});